from contacts.models import Contact, Email
from contacts.serializers import ContactSerializer
from flask import abort, Blueprint, jsonify, make_response, request
from config import db

contacts_bp = Blueprint('contacts', __name__, url_prefix='/contacts')

@contacts_bp.route('/', methods=['POST'])
def add_contact():
	'''
	Creating a contact.
	'''

	serializer = ContactSerializer()
	
	try:
		new_contact = serializer.create(request.json)
	except (ValueError, TypeError) as e:
		abort(make_response(jsonify(message=str(e)), 400))

	return serializer.jsonify(new_contact)

@contacts_bp.route('/', methods=['GET'])
def get_contacts():
	'''
	Returns all registered contacts.
	'''

	serializer = ContactSerializer(many=True)
	all_contacts = Contact.query.all()
	result = serializer.dump(all_contacts)
	return jsonify(result)

@contacts_bp.route('/<string:username>', methods=['GET'])
def get_contact(username):
	'''
	Query a specific contact by username.
	'''

	serializer = ContactSerializer()
	contact = Contact.query.filter(Contact.username == username).first()
	
	if not contact:
		err_msg = 'Contact {username} does not exist.'.format(username=username)
		abort(make_response(jsonify(message=err_msg), 404))

	return serializer.jsonify(contact)

@contacts_bp.route('/<string:username>', methods=['PUT'])
def update_contact(username):
	'''
	Update an existing contact.
	The contact must be speicified by username.
	'''

	serializer = ContactSerializer()
	try:
		contact = serializer.update(username, request.json)
	except (TypeError, ValueError) as e:
		status_code = 404 if 'not exist' in str(e) else 400
		abort(make_response(jsonify(message=str(e)), status_code))

	return serializer.jsonify(contact)

@contacts_bp.route('/<string:username>', methods=['PATCH'])
def partial_update_contact(username):
	'''
	Partial update on already existing contact. 
	Not all fields are needed.
	The contact must be specified by username.
	'''

	serializer = ContactSerializer()
	try:
		contact = serializer.partial_update(username, request.json)
	except (TypeError, ValueError) as e:
		status_code = 404 if 'not exist' in str(e) else 400
		abort(make_response(jsonify(message=str(e)), status_code))

	return serializer.jsonify(contact)


@contacts_bp.route('/<string:username>', methods=['DELETE'])
def delete_contact(username):
	'''
	Deleting an existing contact.
	'''
	
	serializer = ContactSerializer()
	contact = Contact.query.filter(Contact.username == username).first()

	if not contact:
		err_msg = 'Contact {username} does not exist.'.format(username=username)
		abort(make_response(jsonify(message=err_msg), 404))

	db.session.delete(contact)
	db.session.commit()

	return serializer.jsonify(contact)
