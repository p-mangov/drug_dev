from config import db

class Contact(db.Model):
	'''
	Model for the contact object.
	'''

	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(255), unique=True)
	first_name = db.Column(db.String(255))
	last_name = db.Column(db.String(255))
	emails = db.relationship('Email', backref='contacts', cascade='all, delete-orphan')

	def __init__(self, username, first_name, last_name):
		self.username = username
		self.first_name = first_name
		self.last_name = last_name


class Email(db.Model):
	'''
	Model for the email object.
	'''

	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(255), unique=True)
	contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))

	def __init__(self, email, contact_id):
		self.email = email
		self.contact_id = contact_id
