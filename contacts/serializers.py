from config import db, ma
from marshmallow import fields
from .models import Contact, Email

class ContactSerializer(ma.ModelSchema):
	'''
	Serializer class for the Contact model.
	'''
	
	emails = fields.Nested('EmailSerializer', many=True)

	class Meta:
		model = Contact
		fields = (
			'username',
			'first_name',
			'last_name',
			'emails',
		)

	def create(self, data):
		'''
		Create a new contact.
		'''
		validated_data = {}

		# Check if all the needed fields have been sent
		for field in self.Meta.fields:
			validated_data[field] = data.get(field)
			if not validated_data[field]:
				raise ValueError('Field {field} is missing.'.format(field=field))

		# Check if a contact with requested username already exists
		existing_contact = self.Meta.model.query.filter(self.Meta.model.username == validated_data.get('username'))
		if existing_contact.first():
			raise ValueError("A contact with this username already exists.")

		# Extract the emails and check type
		emails = validated_data.pop('emails')
		if type(emails) is not list:
			raise TypeError('The type of the emails field must be a list.')

		email_serializer = EmailSerializer()

		# Create the new contact
		# Save it in order to pass the id to the email serialzier
		new_entry = self.Meta.model(**validated_data)
		db.session.add(new_entry)
		db.session.commit()	

		for email in emails:
			try:
				email_serializer.create({**email, 'contact_id': new_entry.id})
			except ValueError as e:
				# If the email creation has failed, delete the existing contact to 
				# preserve atomicity and raise the error again
				db.session.delete(new_entry)
				db.session.commit()
				raise

		return new_entry

	def update(self, username, data):
		'''
		Update an existing contact.
		'''

		fields = self.Meta.fields

		# Check if all needed fields have been sent
		# If so pass it along to the partial_update
		for field in fields:
			field_data = data.get(field)
			if not field_data:
				raise ValueError('Field {field} is missing.'.format(field=field))

		return self.partial_update(username, data)

	def partial_update(self, username, data):
		'''
		Perform a partial update on an existing contact. Not all fields are needed.
		'''

		contact = Contact.query.filter(Contact.username==username).first()
		# Check if the contact exists
		if not contact:
			raise ValueError('Contact {} does not exist.'.format(username))

		# Check if the requested new username has been taken by another user
		new_username = data.get('username')
		if username:
			existing_contact = Contact.query.filter(Contact.username == new_username).first()
			if existing_contact and contact.id != existing_contact.id:
				raise ValueError('A contact with the username {username} already exists.'.format(username=new_username))

		# Update the fields one by one
		first_name = data.get('first_name')
		if first_name:
			contact.first_name = first_name
		
		last_name = data.get('last_name')
		if last_name:
			contact.last_name = last_name
		
		new_emails = data.get('emails')
		if new_emails:
			if type(new_emails) is not list:
				raise TypeError('The type of the emails field must be a list.')

			email_serializer = EmailSerializer()
			existing_email_addresses = contact.emails
			
			# Delete the existing emails
			for email in existing_email_addresses:
				db.session.delete(Email.query.filter(Email.email == email.email).first())
			
			# Create the new emails
			for email in new_emails:
				try:
					email_serializer.create({**email, 'contact_id': contact.id})
				except ValueError as e:
					raise
			
		db.session.commit()
		
		return contact


class EmailSerializer(ma.Schema):
	'''
	Serialzier class for the email model.
	'''

	class Meta:
		model = Email
		fields = (
			'email',
			)
	
	def create(self, data):
		'''
		Create a new email object.
		'''

		fields = [*self.Meta.fields, 'contact_id']
		validated_data = {}

		for field in fields:
			validated_data[field] = data.get(field)
			if not validated_data[field]:
				raise ValueError('Field {field} is missing.'.format(field=field))

		existing_email = self.Meta.model.query.filter(self.Meta.model.email==validated_data.get('email')).first()

		if existing_email:
			raise ValueError('The email {email} already exists'.format(email=validated_data.get('email')))

		new_entry = Email(**validated_data)
		db.session.add(new_entry)
		db.session.commit()
