Drug Dev Submission by Plamen Mangov
===========

A small fully functional API application allowing to create contacts and attach email objects to them.
In addition it comes with two configured periodic celery tasks creating a user every 15 seconds and deleting all users older than 1 minute.

Requirements
------------

- Virtualenv
- Redis server

Installation
------------

You can create a virtual environment and install the required packages with the following commands:

    $ virtualenv venv
    $ source venv/bin/activate
    (venv) $ pip install -r requirements.txt

Running
-------

With the virtual environment activated you can `cd` into any of the examples and run the following commands.

For running the application:

    (venv) $ flask run

For running the tests:

    (venv) $ pytest

For running the celery broker (given the Redis server is running):

    (venv) $ celery -A app:celery worker -B --loglevel=info

