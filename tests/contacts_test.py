from config import app, db
from contacts.models import Contact
import json
import unittest
import pytest

class TestClass(unittest.TestCase):

	def setUp(self):
		self.app = app
		self.client = self.app.test_client()
		
		self.contacts = [
			{
				'username': 'john.doe',
				'first_name': 'John',
				'last_name': 'Doe',
				'emails': [
					{'email': 'john1@doe.com'},
					{'email': 'john2@doe.com'}
				]
			},
			{
				'username': 'jane.doe',
				'first_name': 'Jane',
				'last_name': 'Doe',
				'emails': [
					{'email': 'jane1@doe.com'},
					{'email': 'jane2@doe.com'}
				]
			}
		]

	def tearDown(self):
		contact_usernames = [contact['username'] for contact in self.contacts]
			
		for username in contact_usernames:
			
			test_contact = Contact.query.filter(Contact.username==username).first()
			if test_contact:
				db.session.delete(test_contact)
				db.session.commit()

	def test_create_contact_ok(self):
		contact = self.contacts[0]
		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		self.assertEqual(json.loads(resp.data), contact )

	def test_create_contact_missing_uname(self):
		contact = self.contacts[0].copy()
		contact.pop('username')
		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field username is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_create_contact_missing_first_name(self):
		contact = self.contacts[0]
		contact.pop('first_name')
		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field first_name is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_create_contact_missing_last_name(self):
		contact = self.contacts[0]
		contact.pop('last_name')
		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field last_name is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_create_contact_missing_emails(self):
		contact = self.contacts[0]
		contact.pop('emails')
		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field emails is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_create_contact_email_wrong_type(self):
		contact = self.contacts[0]
		contact['emails'] = contact['emails'][0]
		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'The type of the emails field must be a list.'}
		self.assertEqual(json.loads(resp.data), expected_response)
	
	def test_create_contact_email_missing_field(self):
		contact = self.contacts[0]
		contact['emails'][0].pop('email')
		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field email is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_find_non_existing_contact(self):
		contact = self.contacts[0]
		contact['username'] = 'wrong_username'
		resp = self.client.get(path='/contacts/' + contact['username'], data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Contact {} does not exist.'.format(contact['username'])}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_create_existing_email(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		existing_email = contact['emails'][0]
		contact = self.contacts[1]
		contact['emails'].append(existing_email)

		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'The email {} already exists'.format(existing_email.get('email'))}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_create_contact_existing_username(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		existing_username = contact['username']
		contact = self.contacts[1]
		contact['username'] = existing_username

		resp = self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'A contact with this username already exists.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_find_existing_contact(self):
		contact = self.contacts[0]
		
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		resp = self.client.get(path='/contacts/' + contact['username'], content_type='application/json')
		self.assertEqual(json.loads(resp.data), contact)

	def test_delete_non_existing_contact(self):
		contact = self.contacts[0]
		contact['username'] = 'wrong_username'
		resp = self.client.delete(path='/contacts/' + contact['username'], content_type='application/json')
		expected_response = {'message': 'Contact {} does not exist.'.format(contact['username'])}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_delete_existing_contact(self):
		contact = self.contacts[0]

		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		resp = self.client.delete(path='/contacts/' + contact['username'], content_type='application/json')
		self.assertEqual(json.loads(resp.data), contact)

	def test_update_non_existing_contact(self):
		contact = self.contacts[0]
		resp = self.client.put(path='/contacts/' + contact['username'], data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Contact {} does not exist.'.format(contact['username'])}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_update_contact_missing_uname(self):
		contact = self.contacts[0].copy()
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		username = contact.pop('username')
		resp = self.client.put(path='/contacts/' + username, data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field username is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_update_contact_missing_first_name(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		contact.pop('first_name')
		resp = self.client.put(path='/contacts/' + contact['username'], data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field first_name is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_update_contact_missing_last_name(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		contact.pop('last_name')
		resp = self.client.put(path='/contacts/' + contact['username'], data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field last_name is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)

	def test_update_contact_missing_emails(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		contact.pop('emails')
		resp = self.client.put(path='/contacts/' + contact['username'], data=json.dumps(contact), content_type='application/json')
		expected_response = {'message': 'Field emails is missing.'}
		self.assertEqual(json.loads(resp.data), expected_response)
	
	def test_partial_update_first_name(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		contact['first_name'] = self.contacts[1]['first_name']
		data = {'first_name': contact['first_name']}
		resp = self.client.patch(path='/contacts/' + contact['username'], data=json.dumps(data), content_type='application/json')

		self.assertEqual(json.loads(resp.data), contact)

	def test_partial_update_last_name(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		contact['last_name'] = self.contacts[1]['last_name']
		data = {'last_name': contact['last_name']}
		resp = self.client.patch(path='/contacts/' + contact['username'], data=json.dumps(data), content_type='application/json')

		self.assertEqual(json.loads(resp.data), contact)
	
	def test_partial_update_fewer_emails(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		contact['emails'] = contact['emails'][:1]
		data = {'emails': contact['emails']}
		resp = self.client.patch(path='/contacts/' + contact['username'], data=json.dumps(data), content_type='application/json')

		self.assertEqual(json.loads(resp.data), contact)

	def test_partial_update_more_emails(self):
		contact = self.contacts[0]
		self.client.post(path='/contacts/', data=json.dumps(contact), content_type='application/json')

		contact['emails'].extend(self.contacts[1]['emails'])
		data = {'emails': contact['emails']}
		resp = self.client.patch(path='/contacts/' + contact['username'], data=json.dumps(data), content_type='application/json')

		self.assertEqual(json.loads(resp.data), contact)


if __name__ == '__main__':
	unnittest.main()
