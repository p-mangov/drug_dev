from config import app, make_celery, db
from contacts.serializers import ContactSerializer
from contacts.models import Contact
from datetime import timedelta
import random

celery = make_celery(app)

# Defining the schedule for the periodic taks and updating celery conf
celery.conf.update(CELERYBEAT_SCHEDULE = {
    'create_user_15s': {
        'task': 'create_user',
        'schedule': timedelta(seconds=15)
    },
    'delete_old_users': {
        'task': 'delete_users',
        'schedule': timedelta(seconds=60)
    },
})

@celery.task(name='create_user')
def create_user():
	'''
	Creating a user John Doe with a random integer between 1 and 1000 as a suffix.
	'''
	
	serializer = ContactSerializer()
	rand_int = str(random.randint(1,1000))
	contact = {
		'username': 'john.doe' + rand_int,
		'first_name': 'John',
		'last_name': 'Doe',
		'emails': [
			{
				'email': 'john{}@doe.com'.format(rand_int)
			}
		]
	}

	new_contact = serializer.create(contact)

	db.session.add(new_contact)
	db.session.commit()

@celery.task(name="delete_users")
def delete_users():
	'''
	Deleting all users.
	'''
	contacts = Contact.query.all()
	for contact in contacts:
		db.session.delete(contact)
	db.session.commit()

# Run Server
if __name__ == '__main__':
	app.run(debug=True)
